<?php


namespace App\Cart;


use App\Models\User;

class Cart
{


    private $user;

    public function __construct($user)
    {
      $this->user=$user;
    }

    public function add($products)
    {
        $products = $this->getStorePayload($products);
        $this->user->cart()->syncWithoutDetaching($products);
    }

    public function update($productId, $quantity)
    {
        $this->user->cart()->updateExistingPivot($productId, [
            'quantity'=>$quantity
        ]);
    }
    public function getStorePayload($products)
    {

        return collect($products)->keyBy('id')->map(function ($product) {
            return [
                'quantity' => $product['quantity']+ $this->getCurrentQuantity($product['id']),
            ];
        })->toArray();
    }

    protected function getCurrentQuantity($productId)
    {
         if ($product= $this->user->cart->where('id', $productId)->first()){
             return  $product->pivot->quantity;
         }
         return 0;
    }

    public function delete($id)
    {
        $this->user->cart()->detach($id);
    }

    public function empty()
    {
        $this->user->cart()->detach();
    }

}
