<?php


namespace App\Cart;


use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\IntlMoneyFormatter;
use Money\Money as BaseMoney;

class Money
{
    private $money;

    public function __construct($value)
    {
        $this->money = new BaseMoney($value, new Currency('KES'));

    }

    public function amount()
    {
         return $this->money->getAmount();
    }

    public function formatted()
    {
        $formatter =new IntlMoneyFormatter(
            new \NumberFormatter('en_KE',\NumberFormatter::CURRENCY),
            new ISOCurrencies()
        );
        return $formatter->format($this->money);
    }
}
