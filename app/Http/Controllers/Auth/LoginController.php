<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\PrivateUserResource;
use http\Env\Response;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function action(LoginRequest $request)
    {
      if (!$token= auth()->attempt($request->only(['password','email']))){
       return response()->json([
           'errors'=>[
               'email'=>['Could Not sign in with those details']
           ]
       ], 422);
      }

       return (new PrivateUserResource($request->user()))
           ->additional([
           'meta'=>['token'=>$token],
       ]);
    }
}
