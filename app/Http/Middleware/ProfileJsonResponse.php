<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;

class ProfileJsonResponse
{
    public function handle($request, Closure $next)
    {
        $response= $next($request);
        if ($response instanceof JsonResponse
            and app('debugbar')->isEnabled()
            and $request->has('_debug'))
        {
            $response->setData($response->getData(true)+[
                   '_debugbar'=>Arr::only(app('debugbar')->getData(),['queries'])
                ]);
        }
            return $response;
    }
}
