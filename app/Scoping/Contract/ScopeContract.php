<?php


namespace App\Scoping\Contract;


use Illuminate\Database\Eloquent\Builder;

interface ScopeContract
{
  public function apply(Builder $builder, $value);
}
