<?php
//filter
namespace App\Scoping;



use App\Scoping\Contract\ScopeContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class Scoper{
    private $request;

    public function __construct(Request $request)
    {
       $this->request= $request;

    }

    public function apply(Builder $builder, array $scopes)
    {

         foreach ($scopes as $key=> $scope){
             if (!$scope instanceof ScopeContract){
                    continue;
             }
             $scope->apply($builder, $this->request->get($key));
         }
         return $builder;
    }
}
