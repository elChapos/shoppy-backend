<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductVariation;
use Faker\Generator as Faker;

$factory->define(ProductVariation::class, function (Faker $faker) {
    return [
        'product_id'=>factory(\App\Models\Product::class)->create()->id,
        'name'=>$faker->unique()->name,
        'product_variation_type_id'=>factory(\App\Models\ProductVariationTypes::class)->create()->id
    ];
});
