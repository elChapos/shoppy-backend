<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Models\Stock::class, function (Faker $faker) {
    return [
        'quantity'=>rand(50,100)
    ];
});
