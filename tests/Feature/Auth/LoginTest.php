<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Hamcrest\Thingy;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    public function test_it_validates_email()
    {
         $this->json('POST', 'api/auth/login')
             ->assertJsonValidationErrors(['email']);
    }
    public function test_it_validates_password()
    {
        $this->json('POST', 'api/auth/login')
            ->assertJsonValidationErrors(['password']);
    }
    public function test_it_rejects_an_invalid_login()
    {
        $user=factory(User::class)->create([
            "email"=>"tezt@example.com",
            "password"=>"password",
        ]);
        $this->json('POST', 'api/auth/login',[
            "email"=>"abc@gmail.com",
            "password"=>"password"
        ])->assertJsonValidationErrors(["email"]);
    }

    public function test_it_returns_a_token_if_credentials_match()
    {
        $user=factory(User::class)->create([
            "email"=>"tezt@example.com",
            "password"=>"password",
        ]);
        $this->json('POST', 'api/auth/login',[
            "email"=>$user->email,
            "password"=>"password"
        ])->assertJsonStructure([
            'meta'
        ])->assertStatus(200);
    }
}
