<?php

namespace Tests\Feature\Auth;


use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    public function test_it_requires_a_name()
    {
        $this->json('POST', 'api/auth/register',['name'=>''])
            ->assertJsonValidationErrors(['name']);
    }

    public function test_it_requires_a_email()
    {
        $this->json('POST', 'api/auth/register',['email'=>'xyz'])
            ->assertJsonValidationErrors(['name']);
    }

    public function test_it_requires_a_password()
    {
        $this->json('POST', 'api/auth/register',['password'=>''])
            ->assertJsonValidationErrors(['name']);
    }

    public function test_it_requires_a_unique_email()
    {
        $user= factory(User::class)->create([
             'email'=>'sample@example.com'
        ]);
        $this->json('POST', 'api/auth/register',['email'=>$user->email])
            ->assertJsonValidationErrors(['name']);
    }

    public function test_it_registers_a_user()
    {
        $this->json('POST', 'api/auth/register',['name'=>'Johnny Junior','email'=>$email='johny@gmail.com','password'=>'password'])

            ->assertStatus(201);

        $this->assertDatabaseHas('users', [
            'email'=>$email
        ]);
    }

    public function test_it_returns_a_user_resource()
    {
        $this->json('POST', 'api/auth/register',['name'=>'Johnny Junior','email'=>$email='johny@gmail.com','password'=>'password'])

            ->assertJsonFragment([
                'email'=>$email
            ]);


    }
}
