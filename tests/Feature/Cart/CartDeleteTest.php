<?php

namespace Tests\Feature\Cart;

use App\Models\ProductVariation;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CartDeleteTest extends TestCase
{
    public function test_it_fails_if_unauthenticated()
    {
        $this->json('DELETE', 'api/cart/1')
            ->assertStatus(401);
    }


    public function test_it_fails_if_product_is_not_found()
    {
        $user=factory(User::class)->create();
        $this->jsonAs($user,'DELETE', 'api/cart/1')
            ->assertStatus(404);
    }

    public function test_it_can_delete_an_item()
    {
        $user=factory(User::class)->create();
        $user->cart()->attach(
            $product=factory(ProductVariation::class)->create(),[
                'quantity'=>$quantity=5
            ]
        );

        $this->jsonAs($user,'DELETE', 'api/cart/'.$product->id);

        $this->assertDatabaseMissing('cart_user',[
            'product_variation_id'=>$product->id,
            'quantity'=>$quantity
        ]);

    }
}
