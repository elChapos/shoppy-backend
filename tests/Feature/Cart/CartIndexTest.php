<?php

namespace Tests\Feature\Cart;

use App\Models\ProductVariation;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CartIndexTest extends TestCase
{
    public function test_it_fails_if_unauthenticated()
    {
        $this->json('GET', 'api/cart')
            ->assertStatus(401);
    }

    public function test_it_show_products_in_users_cart()
    {
        $user = factory(User::class)->create();
        $user->cart()->attach(
            $product=factory(ProductVariation::class)->create(),[
                'quantity'=>$quantity=5
            ]
        );
        $this->jsonAs($user, 'GET', 'api/cart')->assertJsonFragment(["quantity"=>5]);
    }
}
