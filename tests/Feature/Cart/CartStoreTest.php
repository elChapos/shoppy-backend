<?php

namespace Tests\Feature\Cart;

use App\Models\ProductVariation;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CartStoreTest extends TestCase
{
    public function test_it_fails_if_unauthenticated()
    {
        $this->json('POST', 'api/cart')
            ->assertStatus(401);
    }


    public function test_it_requires_products()
    {
        $user = factory(User::class)->create();
        $this->jsonAs($user, 'POST', 'api/cart')
            ->assertJsonValidationErrors(["products"]);
    }

    public function test_it_requires_products_to_be_an_array()
    {
        $user = factory(User::class)->create();
        $this->jsonAs($user, 'POST', 'api/cart', [
            'products' => 1
        ])
            ->assertJsonValidationErrors(["products"]);
    }

    public function test_it_requires_each_product_variation_has_id()
    {
        $user = factory(User::class)->create();
        $this->jsonAs($user, 'POST', 'api/cart', [
            'products' => [
                ['quantity' => 1]
            ]
        ])->assertJsonValidationErrors(["products.0.id"]);
    }

    public function test_it_requires_each_product_variation_to_exist()
    {
        $user = factory(User::class)->create();
        $this->jsonAs($user, 'POST', 'api/cart', [
            'products' => [
                ['quantity' => 1, 'id' => 1]
            ]
        ])->assertJsonValidationErrors(["products.0.id"]);
    }

    public function test_it_requires_each_product_variation_quantity_to_be_numeric()
    {
        $user = factory(User::class)->create();
        $this->jsonAs($user, 'POST', 'api/cart', [
            'products' => [
                ['quantity' => 'one', 'id' => 1]
            ]
        ])->assertJsonValidationErrors(["products.0.quantity"]);
    }

    public function test_it_requires_each_product_variation_quantity_to_be_at_least_1()
    {
        $user = factory(User::class)->create();
        $this->jsonAs($user, 'POST', 'api/cart', [
            'products' => [
                ['quantity' => -1, 'id' => 1]
            ]
        ])->assertJsonValidationErrors(["products.0.quantity"]);
    }

    public function test_it_adds_products_to_cart()
    {
        $user = factory(User::class)->create();
        $product = factory(ProductVariation::class)->create();
        $response = $this->jsonAs($user, 'POST', 'api/cart', [
            'products' => [
                ['quantity' => 2, 'id' => $product->id]
            ]
        ]);
         $this->assertDatabaseHas('cart_user',['product_variation_id'=>$product->id, 'quantity'=>2]);

    }


}
