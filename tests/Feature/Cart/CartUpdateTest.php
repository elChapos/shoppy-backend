<?php

namespace Tests\Feature\Cart;

use App\Models\ProductVariation;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CartUpdateTest extends TestCase
{

    public function test_it_fails_if_unauthenticated()
    {
        $this->json('PATCH', 'api/cart/1')
            ->assertStatus(401);
    }

    public function test_it_fails_if_product_is_not_found()
    {
        $user=factory(User::class)->create();
        $this->jsonAs($user,'PATCH', 'api/cart/1')
            ->assertStatus(404);
    }

    public function test_it_requires_quantity()
    {
        $user=factory(User::class)->create();
        $product=factory(ProductVariation::class)->create();

        $this->jsonAs($user,'PATCH', 'api/cart/'.$product->id)
             ->assertJsonValidationErrors(["quantity"]);
    }

    public function test_it_requires_quantity_to_be_numeric()
    {
        $user=factory(User::class)->create();
        $product=factory(ProductVariation::class)->create();

        $this->jsonAs($user,'PATCH', 'api/cart/'.$product->id,['quantity'=>''])
            ->assertJsonValidationErrors(["quantity"]);
    }


    public function test_it_requires_quantity_to_be_greater_than_0()
    {
        $user=factory(User::class)->create();
        $product=factory(ProductVariation::class)->create();

        $this->jsonAs($user,'PATCH', 'api/cart/'.$product->id,['quantity'=>0])
            ->assertJsonValidationErrors(["quantity"]);
    }

    public function test_it_can_update_quantity()
    {
        $user=factory(User::class)->create();
        $user->cart()->attach(
            $product=factory(ProductVariation::class)->create(),[
                'quantity'=>$quantity=5
            ]
        );

        $this->jsonAs($user,'PATCH', 'api/cart/'.$product->id,['quantity'=>5]);

        $this->assertDatabaseHas('cart_user',[
            'product_variation_id'=>$product->id,
            'quantity'=>$quantity
        ]);

    }
}
