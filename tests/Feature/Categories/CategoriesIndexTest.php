<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoriesIndexTest extends TestCase
{

    public function test_it_returns_a_collection_of_categories()
    {
        $categories = factory(Category::class, 2)->create();

        $this->json('GET', 'api/categories')
            ->assertJsonFragment([
                'slug' => $categories->get(0)->slug
               ],
                [
                    'slug' => $categories->get(1)->slug
                ]);
    }

    public function test_response_is_array_items()
    {
        $categories = factory(Category::class, 2)->create();
        $response=$this->json('GET', 'api/categories');

        $categories->each(function ($category) use ($response){
            $response->assertJsonFragment([
                'slug'=>$category->slug
            ]);
        });
    }

    public function test_it_returns_only_parent_categories()
    {
        $category = factory(Category::class)->create();

        $category->children()->save(
            factory(Category::class)->create()
        );
        $this->json('GET', 'api/categories')
            ->assertJsonCount(1,'data');

    }


    public function test_it_returns_categories_in_order()
    {

        $category_a = factory(Category::class)->create(['order'=>2]);
        $category_b = factory(Category::class)->create(['order'=>1]);

        $this->json('GET', 'api/categories')
            ->assertSeeInOrder([
                $category_b->slug,
                $category_a->slug
            ]);

    }


}
