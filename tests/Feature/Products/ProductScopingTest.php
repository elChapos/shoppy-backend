<?php

namespace Tests\Feature\Products;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductScopingTest extends TestCase
{
    public function test_can_scope_by_category()
    {
        $product_1 = factory(Product::class)->create();
        $product_1->categories()->save(
            $category=factory(Category::class)->create()
        );

        $product_2 = factory(Product::class)->create();
        $this->json('GET', "api/products?category={$category->slug}")
            ->assertJsonCount(1,"data");



    }
}
