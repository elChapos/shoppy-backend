<?php

namespace Tests\Unit\Cart;

use App\Cart\Cart;
use App\Models\ProductVariation;
use App\Models\User;
use Tests\TestCase;

class CartTest extends TestCase
{
    public function test_can_add_products_to_cart()
    {
       $cart = new Cart($user = factory(User::class)->create());

       $product= factory(ProductVariation::class)->create();


       $cart->add([
           ['id'=>$product->id, 'quantity'=>1]
       ]);

       $this->assertCount(1, $user->fresh()->cart);


    }

    public function test_can_update_products_in_cart()
    {

        $product= factory(ProductVariation::class)->create();
        $cart = new Cart($user = factory(User::class)->create());

        $cart->add([
            ['id'=>$product->id, 'quantity'=>1]
        ]);

        $cart = new Cart($user->fresh());
        $cart->add([
            ['id'=>$product->id, 'quantity'=>1]
        ]);

        $this->assertEquals( $user->fresh()->cart->first()->pivot->quantity,2);


    }

    public function test_can_update_product_quantities_in_the_cart()
    {
        $cart = new Cart($user = factory(User::class)->create());

        $user->cart()->attach( $product= factory(ProductVariation::class)->create(),['quantity'=>1] );

        $cart->update($product->id, 3);

        $this->assertEquals($user->fresh()->cart->first()->pivot->quantity, 3);
    }

    public function test_can_delete_product_from_the_cart()
    {
        $cart = new Cart($user = factory(User::class)->create());

        $user->cart()->attach( $product= factory(ProductVariation::class)->create(),['quantity'=>1] );

        $cart->delete($product->id);

        $this->assertCount(0,$user->fresh()->cart );
    }

    public function test_it_can_empty_cart()
    {
        $cart = new Cart($user = factory(User::class)->create());

        $user->cart()->attach( $product= factory(ProductVariation::class)->create(),['quantity'=>1] );

        $cart->empty();

        $this->assertCount(0,$user->fresh()->cart );
    }
}
