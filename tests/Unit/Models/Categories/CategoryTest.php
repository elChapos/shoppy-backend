<?php

namespace Tests\Unit\Models\Categories;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;


class CategoryTest extends TestCase
{

    public function test_it_has_many_children()
    {
        $category = factory(Category::class)->create();
        $category->children()->save(
            factory(Category::class)->create()
        );
        $this->assertInstanceOf(Category::class,$category->children->first());
//       $this->assertInstanceOf(Collection::class,$category->children());
    }

    public function test_it_can_only_fetch_parents()
    {
        $category = factory(Category::class)->create();
        $category->children()->save(
            factory(Category::class)->create()
        );
        $this->assertEquals(1, Category::parents()->count());
    }

    public function test_is_orderable_by_a_numbered_order()
    {
        $category_a = factory(Category::class)->create(['order'=>2]);
        $category_b = factory(Category::class)->create(['order'=>1]);

        $this->assertEquals($category_b->name, Category::ordered()->first()->name);
    }

    public function test_it_has_many_products()
    {
        $category= factory(Category::class)->create(['order'=>1]);
        $category->products()->save(
             factory(Product::class)->create()
        );
        $this->assertInstanceOf(Product::class, $category->products->first());
    }
}
