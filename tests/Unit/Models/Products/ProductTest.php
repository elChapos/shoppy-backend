<?php

namespace Tests\Unit\Products;

use App\Cart\Money;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductVariation;
use App\Models\Stock;
use Tests\TestCase;

class ProductTest extends TestCase
{
    public function test_it_users_slug_for_route_key()
    {
        $product= new Product();
        $this->assertEquals($product->getRouteKeyName(),'slug');
    }

    public function test_it_has_many_categories()
    {
        $product = factory(Product::class)->create();

        $product->categories()->save(
            factory(Category::class)->create()
        );

        $this->assertInstanceOf(Category::class, $product->categories->first());
    }
    public function test_it_has_many_variations()
    {
        $product = factory(Product::class)->create();

        $product->variations()->save(
            factory(ProductVariation::class)->create([
                'product_id'=>$product->id
            ])
        );

        $this->assertInstanceOf(ProductVariation::class, $product->variations->first());
    }

    public function  test_it_returns_a_money_instance_for_the_price()
    {
       $product =factory(Product::class)->create();
       $this->assertInstanceOf(Money::class, $product->price);
    }

    public function  test_it_returns_a_formatted_price()
    {
        $product =factory(Product::class)->create(['price'=>1000]);
        $this->assertEquals('Ksh10.00', $product->formattedPrice);

    }

    public function  test_it_returns_product_quantity_in_stock()
    {
        $product =factory(Product::class)->create(['price'=>1000]);

        $product->variations()->save(
            $variation_1=factory(ProductVariation::class)->make()
        );
        $variation_1->stocks()->save(factory(Stock::class)->make(['quantity'=>6]));


        $product->variations()->save(
            $variation_2=factory(ProductVariation::class)->make()
        );
        $variation_2->stocks()->save(factory(Stock::class)->make(['quantity'=>7]));

        $this->assertEquals(13, $product->stockCount());
    }

    public function  test_it_checks_if_product_in_stock()
    {
        $product =factory(Product::class)->create(['price'=>1000]);

        $product->variations()->save(
            $variation_1=factory(ProductVariation::class)->make()
        );
        $variation_1->stocks()->save(factory(Stock::class)->make(['quantity'=>6]));
        $product->variations()->save(
            $variation_2=factory(ProductVariation::class)->make()
        );
        $variation_2->stocks()->save(factory(Stock::class)->make(['quantity'=>7]));

        $this->assertTrue($product->inStock());
    }



}
