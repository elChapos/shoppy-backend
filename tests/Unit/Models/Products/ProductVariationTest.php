<?php

namespace Tests\Unit\Products;

use App\Cart\Money;
use App\Models\Product;
use App\Models\ProductVariation;
use App\Models\ProductVariationTypes;
use App\Models\Stock;
use Tests\TestCase;

class ProductVariationTest extends TestCase
{
    public function test_it_has_one_variation_type()
    {
       $variation=factory(ProductVariation::class)->create();
       $this->assertInstanceOf(ProductVariationTypes::class, $variation->type);
    }

    public function test_it_belongs_to_product()
    {
        $variation=factory(ProductVariation::class)->create();

        $this->assertInstanceOf(Product::class, $variation->product);
    }

    public function  test_it_returns_a_money_instance_for_the_price()
    {
        $variation =factory(ProductVariation::class)->create();
        $this->assertInstanceOf(Money::class, $variation->price);
    }

    public function  test_it_returns_a_formatted_price()
    {
        $product =factory(ProductVariation::class)->create(['price'=>1000]);
        $this->assertEquals('Ksh10.00', $product->formattedPrice);

    }

    public function test_can_get_base_product_price_if_variation_has_null_price()
    {
        $variation = factory(ProductVariation::class)->create();
        $this->assertEquals($variation->price->amount(), $variation->product->price->amount());
    }

    public function test_it_has_may_stocks()
    {
         $product_variation = factory(ProductVariation::class)->create();
         $product_variation->stocks()->save(
             factory(Stock::class)->make()
         );

         $this->assertInstanceOf(Stock::class, $product_variation->stocks->first());
    }

    public function test_that_it_has_stock_information()
    {
        $product_variation = factory(ProductVariation::class)->create();
        $product_variation->stocks()->save(
            factory(Stock::class)->make()
        );

        $this->assertInstanceOf(ProductVariation::class, $product_variation->stock->first());
    }
    public function test_that_it_has_stock_count_pivot_within_stock_information()
    {
        $product_variation = factory(ProductVariation::class)->create();
        $product_variation->stocks()->save(
            factory(Stock::class)->make([
                'quantity'=>$quantity=5
            ])
        );

        $this->assertEquals($product_variation->stock->first()->pivot->stock,$quantity);
    }

    public function test_that_it_has_in_stock_pivot_within_stock_information()
    {
        $product_variation = factory(ProductVariation::class)->create();
        $product_variation->stocks()->save(
            factory(Stock::class)->make([
                'quantity'=>$quantity=5
            ])
        );

        $this->assertEquals($product_variation->stock->first()->pivot->in_stock, 1);
    }

    public function test_that_it_can_check_if_it_is_in_stock()
    {
        $product_variation = factory(ProductVariation::class)->create();
        $product_variation->stocks()->save(
            factory(Stock::class)->make([
                'quantity'=>$quantity=5
            ])
        );

        $this->assertTrue($product_variation->inStock());
    }

    public function test_that_it_can_count_stock_quantity()
    {
        $product_variation = factory(ProductVariation::class)->create();
        $product_variation->stocks()->save(
            factory(Stock::class)->make([
                'quantity'=>$quantity=5
            ])
        );

        $this->assertEquals($product_variation->stockCount(),$quantity);
    }



}
