<?php

namespace Tests\Unit\Models\Users;

use App\Models\ProductVariation;
use App\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{

    public function test_it_hashes_passwords()
    {
        $user=factory(User::class)->create([
            'password'=>'password'
        ]);
        $this->assertNotEquals($user->password, 'password');
    }

    public function test_it_has_many_cart_products()
    {

        $user=factory(User::class)->create();

        $user->cart()->attach(
            factory(ProductVariation::class)->create()
        );
        $this->assertInstanceOf(ProductVariation::class, $user->cart->first());
    }

    public function test_it_has_quantity_for_each_cart_products()
    {

        $user=factory(User::class)->create();

        $user->cart()->attach(
            factory(ProductVariation::class)->create(),['quantity'=>$quantity=5]
        );
        $this->assertEquals($user->cart->first()->pivot->quantity,$quantity);
    }
}
